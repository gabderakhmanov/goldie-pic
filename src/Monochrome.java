import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Monochrome {

	public static void getBlackAndWhiteIMG(String pathToColorIMG) throws IOException{
		BufferedImage coloredImage = ImageIO.read(new File(pathToColorIMG));
		BufferedImage blackNWhite = new BufferedImage(coloredImage.getWidth(),coloredImage.getHeight(),BufferedImage.TYPE_BYTE_BINARY);
		Graphics2D graphics = blackNWhite.createGraphics();
		graphics.drawImage(coloredImage, 1, 1, null);

		ImageIO.write(blackNWhite, "png", new File("newBlackNWhite.png"));
		System.out.println("Mono IMG ok");
	}
	
}
