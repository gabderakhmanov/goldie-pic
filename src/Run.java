import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.abg.db.DataBaseConnection;
import com.abg.db.WriteToDB;
import com.abg.fileproc.UpdateLineINLinksFile;
import com.abg.getpocket.SendGetPOST;
import com.abg.parsing.HumaningTextProc;
import com.abg.parsing.ParsSERP;
import com.abg.screenshot.ScreenShotMaker;
import com.abg.serp.SerpProcessor;
import com.abg.services.StringFactory;
import com.abg.webdriver.WebDriverSetter;

import junit.framework.Assert;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;

public class Run {

	public static void main(String[] args) throws IOException, HeadlessException, AWTException, InterruptedException, ParseException, SQLException {
		
		String reqFile = "req.txt";

		try (BufferedReader br = new BufferedReader(new FileReader(reqFile))) {

			String query;

			while ((query = br.readLine()) != null && !query.isEmpty()) {
				
				if (!query.contains("*")) {
					
					System.out.println("******* Start processing query " + query + " *****");
					
					//save SERP html file with link for pocket
					String htmlWithLinkSERP = SerpProcessor.saveBingSERPHtml(3400, query);
					
					//get link from htmlWithLinkSERP
					String linkForPocketFromSERP = SerpProcessor.getLinkFromSERPForPocket(htmlWithLinkSERP);
					
					//send to pocket and get id
					//String url = "";
					
					//String url = "";
					String itemID = SendGetPOST.addToPocketAndGetID(linkForPocketFromSERP);
					
					//open the home page of the site
					//set web driver
					ChromeDriver driver = WebDriverSetter.setChromeDriver();
					Thread.sleep(2000);
					driver.get("https://getpocket.com/a/read/"+itemID+"");
					//driver.get("https://getpocket.com/a/read/562597097");
					Thread.sleep(4000);
					
					//save screenshot data to DB
					//DB data local
					String urlDB = "jdbc:mysql://127.0.0.1:3306/goldiepic?useUnicode=true&characterEncoding=UTF-8";
					String userDB = "goldiepic";
					String passwordDB = "goldiepic";
					
					//get raw text from SERP
					ArrayList<String> arrayFromSERPtext = ParsSERP.getTextFromSERP(query);
					for (String string : arrayFromSERPtext) {
						System.out.println("STR>> " +query+ " >> " + string);
					}
					
					//connect
					Connection connect = DataBaseConnection.connectToDB(urlDB, userDB, passwordDB);
					
					//prepearing for write to db human text
					String headerOfPost = StringFactory.upCaseFirstChar(query);
					String urlOfPost = StringFactory.getUrlFromString(query);
					
					//h1 from pocket
					String h1FromPocketOfPost = ScreenShotMaker.getH1FromPocket(driver, connect, urlOfPost);
					
					//make screenshots
					ArrayList<String> arrayOfPostImg = ScreenShotMaker.makeScreen(driver, connect, urlOfPost);
	
					//humaning text for site A
					String humanTextForSiteAFromSERP = HumaningTextProc.getHumanTextForSiteA(arrayFromSERPtext);
					
					//add h2, h3 from pocket to human text
					ArrayList<String> allH2H3array = ScreenShotMaker.getAllH2H3FromPocket(driver, connect, urlOfPost);
			
					String textForSiteWithH2H3 = HumaningTextProc.getHumanTextForSiteAWithH2H3(humanTextForSiteAFromSERP, allH2H3array);
					
					//write to db h1, human text and filenames of IMG
					String postId = WriteToDB.writeToTablePostAndGetLastPostId(connect, headerOfPost, textForSiteWithH2H3, urlOfPost, h1FromPocketOfPost);
					for (String imgFileName : arrayOfPostImg) {
						WriteToDB.writeToTableImgofpost(connect, imgFileName, postId);
					}
					
					connect.close();
					driver.close();
					
					UpdateLineINLinksFile.updateLine(reqFile, query, "*" + query);
				} else {
					System.out.println("link contains * ");
				}
			}
		}
		
		System.out.println("*** ALL FILES ARE SUCCESSFULLY PROCESSED  ***");
	
	}
}
