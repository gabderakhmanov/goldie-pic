import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;

public class ImageQualityReducer {

	public static void makeReduceImg(String srcPath, String destPath, float quality) throws IOException {
  
        Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("jpeg");  
  
        ImageWriter writer = (ImageWriter)iter.next();  
  
        ImageWriteParam iwp = writer.getDefaultWriteParam();  
  
        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);  
  
        iwp.setCompressionQuality(quality);  
  
        File file = new File(destPath);  
        FileImageOutputStream output = new FileImageOutputStream(file);  
        writer.setOutput(output);  
  
        FileInputStream inputStream = new FileInputStream(srcPath);  
        BufferedImage originalImage = ImageIO.read(inputStream);  
  
        IIOImage image = new IIOImage(originalImage, null, null);  
        writer.write(null, image, iwp);  
        writer.dispose();  
        
     
  
        System.out.println("DONE");  

	}

}
