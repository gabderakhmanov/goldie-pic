package com.abg.screenshot;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.Locatable;


public class ScreenShotMaker {

	public static ArrayList<String> makeScreen(WebDriver driver, Connection connect, String urlOfPost) throws IOException, SQLException {
		
		ArrayList<String> arrayOfPostImg = new ArrayList<String>();
		
		// get screenshot
		WebElement rootWebElement = driver.findElement(By.cssSelector(".text_body"));
		WebElement all = rootWebElement.findElement(By.xpath("*"));
		
		//set background and links color
		JavascriptExecutor jseo = (JavascriptExecutor) driver;
		jseo.executeScript("scroll(0, 0)");
		jseo.executeScript("document.body.style.backgroundColor = 'white'");
		
		jseo.executeScript(
		        "var inputs = document.getElementsByTagName('a');" +
		        "for(var i = 0; i < inputs.length; i++) { " +
		        "    inputs[i].style.color = '#313131';" +
		        "}" );
		
		System.out.println(all.findElements(By.xpath("*")));

		List<WebElement> elements = all.findElements(By.xpath("*"));
		for (int i = 0; i < elements.size(); i++) {
			WebElement element = elements.get(i);

			System.out.println(element.getTagName());
			System.out.println(element.getText());
			
			//scroll element to height 51
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			int height51 = element.getLocation().getY() - 51;
			jse.executeScript("scroll(0, " + height51 + ")");
			
			Boolean scrollResp = (Boolean)jse.executeScript("if (document.body.scrollHeight == document.body.scrollTop + window.innerHeight) { return true; } else { return false; }");
			
			
			System.out.println("scrollResp: "+scrollResp.toString());
			// Take screenshot and save to file
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

			// Create instance of BufferedImage from captured screenshot
			BufferedImage img = ImageIO.read(screenshot);
			
			//get screen resolution
			int screenHight = driver.manage().window().getSize().getHeight();
			
			// Get the Height and Width of WebElement
			int height = element.getSize().height;
			int width = element.getSize().width;

			// Create Rectangle using Height and Width to get size
			Rectangle rect = new Rectangle(width, height);

			// Get location of WebElement in a Point
			Point p = element.getLocation();
			// This will provide X & Y co-ordinates of the WebElement

			// calculate res height
			int resHeight = screenHight - rect.height;
			
			System.out.println("rect.height"+ rect.height);
			System.out.println("screenHight"+screenHight);
			System.out.println("resHeight"+resHeight);
			
			if(scrollResp.toString() == "false") {
				
				if (rect.height > 0 && rect.width > 0 && resHeight > 102) {
					
					BufferedImage dest = img.getSubimage(p.getX(), 51, rect.width, rect.height);
					// This will give image data specific to the WebElement

					if (element.getAttribute("class").contains("RIL_IMG") || element.getAttribute("class").contains("caption")) {
						// Write back the image data into a File object
						ImageIO.write(dest, "jpg", screenshot);

						// Copy the file to system ScreenshotPath
						String imgFileName = urlOfPost +"-"+ i + ".jpg";
						FileUtils.copyFile(screenshot, new File(imgFileName));
						
						arrayOfPostImg.add(imgFileName);
						
					} else if (element.getTagName().contains("iframe") && element.getAttribute("src").contains("youtube")) {
						System.out.println("video");
						System.out.println("это че link"+element.getAttribute("src"));
						arrayOfPostImg.add(element.getAttribute("src"));
					} else {
						if(!element.getText().isEmpty()){
							// Write back the image data into a File object
							ImageIO.write(dest, "png", screenshot);

							// Copy the file to system ScreenshotPath
							String imgFileName = urlOfPost +"-"+ i + ".png";
							FileUtils.copyFile(screenshot, new File(imgFileName));
							
							arrayOfPostImg.add(imgFileName);
						}
					}

					
				} else {
					System.out.println("Too big height of element. Res: " + screenHight+ " Elemem height:" + 51+rect.height);
				}
			} else {
				System.out.println("морось в конце");
				
				if (rect.height > 0 && rect.width > 0) {
					System.out.println("в мороси p.getX="+p.getX());
					System.out.println("в мороси  p.getY()="+ p.getY());
					WebElement el = elements.get(i);
					Locatable elementLocation = (Locatable) el;
					System.out.println("в мороси  getY inViewPort "+ elementLocation.getCoordinates().inViewPort().getY());
					System.out.println("в мороси  getX inViewPort "+ elementLocation.getCoordinates().inViewPort().getX());
					
					BufferedImage dest = img.getSubimage(p.getX(), elementLocation.getCoordinates().inViewPort().getY(), rect.width, rect.height);
					// This will give image data specific to the WebElement

					if (element.getAttribute("class").contains("RIL_IMG") || element.getAttribute("class").contains("caption")) {
						// Write back the image data into a File object
						ImageIO.write(dest, "jpg", screenshot);

						// Copy the file to system ScreenshotPath
						String imgFileName = urlOfPost +"-"+ i + ".jpg";
						FileUtils.copyFile(screenshot, new File(imgFileName));
						
						arrayOfPostImg.add(imgFileName);
						
					} else if (element.getTagName().contains("iframe") && element.getAttribute("src").contains("youtube")){
						System.out.println("video");
						System.out.println("это че link"+element.getAttribute("src"));
						arrayOfPostImg.add(element.getAttribute("src"));
						
					} else {
						if(!element.getText().isEmpty()){
							// Write back the image data into a File object
							ImageIO.write(dest, "png", screenshot);

							// Copy the file to system ScreenshotPath
							String imgFileName = urlOfPost +"-"+ i + ".png";
							FileUtils.copyFile(screenshot, new File(imgFileName));
							
							arrayOfPostImg.add(imgFileName);
						}
					}
					
				} else {
					System.out.println("Too big height of element. Res: " + screenHight+ " Elemem height:" + 51+rect.height);
				}
			}
			System.out.println("--------------------------");
		}
		
		return arrayOfPostImg;
	}
	
	public static String getH1FromPocket(WebDriver driver, Connection connect, String urlOfPost) {
		
		WebElement h1WebElement = driver.findElement(By.tagName("h1"));
		String h1Text = h1WebElement.getText();
		
		return h1Text;		
	}

	public static ArrayList<String> getAllH2H3FromPocket(WebDriver driver, Connection connect, String urlOfPost) {
		
		ArrayList<String> arrayOfH2H3WebElements = new ArrayList<String>();
		
		//get h2 from pocket page
		List <WebElement> h2WebElements = driver.findElements(By.tagName("h2"));
		for (WebElement h2webElement : h2WebElements) {
			if(!h2webElement.getText().isEmpty()){
				System.out.println("у н2 такие:"+h2webElement.getText());
				arrayOfH2H3WebElements.add(h2webElement.getText());
			} 
		}
		
		//get h3 from pocket page
		List <WebElement> h3WebElements = driver.findElements(By.tagName("h3"));
		for (WebElement h3webElement : h3WebElements) {
			if(!h3webElement.getText().isEmpty()){
				System.out.println("у н3 такие:"+h3webElement.getText());
				arrayOfH2H3WebElements.add(h3webElement.getText());
			} 
		}
		
		
		return arrayOfH2H3WebElements;		
	}
}
