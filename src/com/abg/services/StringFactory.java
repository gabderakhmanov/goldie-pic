package com.abg.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class StringFactory {
    
	public static String upCaseFirstChar(String line) {
 	   return Character.toUpperCase(line.charAt(0)) + line.substring(1);
	}
	
	
	public static String getUrlFromString(String line) {
		
		StringFactory strgfctry = new StringFactory();
		String transliterateString = strgfctry.transliterate(line);
		
		String transliterateStringWithDoubleDefis = transliterateString.replaceAll("[^a-zA-Z0-9]", "-").toLowerCase();
		String transliterateStringWithDoubleDefisTwo = transliterateStringWithDoubleDefis.replaceAll("([^a-zA-Z0-9])\\1", "-");
		String urlForSiteB = transliterateStringWithDoubleDefisTwo.replaceAll("([^a-zA-Z0-9])\\1", "").replaceAll("^[^a-zA-Z0-9\\s]+|[^a-zA-Z0-9\\s]+$", "");							
		System.out.println("URL for site B : " + urlForSiteB);
		
		return urlForSiteB;
	}
    
	public static String getRandReqFromFile(File reqFile) throws FileNotFoundException
	  {
	     String req = null;
	     Random rand = new Random();
	     int n = 0;
	     for(Scanner sc = new Scanner(reqFile); sc.hasNext(); )
	     {
	        ++n;
	        String line = sc.nextLine();
	        if(rand.nextInt(n) == 0)
	           req = line;         
	     }

	     return req;      
	  }
	
	private static final Map<Character, String> charMap = new HashMap<Character, String>(); {
   	 	
    	charMap.put('А', "A");
        charMap.put('Б', "B");
        charMap.put('В', "V");
        charMap.put('Г', "G");
        charMap.put('Д', "D");
        charMap.put('Е', "E");
        charMap.put('Ё', "E");
        charMap.put('Ж', "Zh");
        charMap.put('З', "Z");
        charMap.put('И', "I");
        charMap.put('Й', "I");
        charMap.put('К', "K");
        charMap.put('Л', "L");
        charMap.put('М', "M");
        charMap.put('Н', "N");
        charMap.put('О', "O");
        charMap.put('П', "P");
        charMap.put('Р', "R");
        charMap.put('С', "S");
        charMap.put('Т', "T");
        charMap.put('У', "U");
        charMap.put('Ф', "F");
        charMap.put('Х', "H");
        charMap.put('Ц', "C");
        charMap.put('Ч', "Ch");
        charMap.put('Ш', "Sh");
        charMap.put('Щ', "Sh");
        charMap.put('Ъ', "");
        charMap.put('Ы', "Y");
        charMap.put('Ь', "");
        charMap.put('Э', "E");
        charMap.put('Ю', "U");
        charMap.put('Я', "Ya");
        charMap.put('а', "a");
        charMap.put('б', "b");
        charMap.put('в', "v");
        charMap.put('г', "g");
        charMap.put('д', "d");
        charMap.put('е', "e");
        charMap.put('ё', "e");
        charMap.put('ж', "zh");
        charMap.put('з', "z");
        charMap.put('и', "i");
        charMap.put('й', "i");
        charMap.put('к', "k");
        charMap.put('л', "l");
        charMap.put('м', "m");
        charMap.put('н', "n");
        charMap.put('о', "o");
        charMap.put('п', "p");
        charMap.put('р', "r");
        charMap.put('с', "s");
        charMap.put('т', "t");
        charMap.put('у', "u");
        charMap.put('ф', "f");
        charMap.put('х', "h");
        charMap.put('ц', "c");
        charMap.put('ч', "ch");
        charMap.put('ш', "sh");
        charMap.put('щ', "sh");
        charMap.put('ъ', "");
        charMap.put('ы', "y");
        charMap.put('ь', "");
        charMap.put('э', "e");
        charMap.put('ю', "u");
        charMap.put('я', "ya");
   }

   private String transliterate(String string) {
       StringBuilder transliteratedString = new StringBuilder();
       for (int i = 0; i < string.length(); i++) {
           Character ch = string.charAt(i);
           String charFromMap = charMap.get(ch);
           if (charFromMap == null) {
               transliteratedString.append(ch);
           } else {
               transliteratedString.append(charFromMap);
           }
       }
       return transliteratedString.toString();
   }
}
