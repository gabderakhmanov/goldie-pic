package com.abg.services;

public class DotPaste {
	public static String getClearStringWithDot(String text) {
		
		String doneString = "";
		if (text.endsWith(",") || text.endsWith(";") || text.endsWith("-") || text.endsWith("–")  || text.endsWith(":") || text.endsWith("_")) {

			String clearString = text.substring(0, text.length()-1);
			doneString = clearString+ ". ";

			return doneString;
			
		} else if (text.endsWith(", ") || text.endsWith("; ") || text.endsWith("- ") || text.endsWith("– ")  || text.endsWith(": ") || text.endsWith("_ "))  {
			
			String clearString = text.substring(0, text.length()-2);
			doneString = clearString+ ". ";
			
			return doneString;
		}
		
		return doneString;
		
	}

}
