package com.abg.getpocket;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class SendGetPOST {



	public static String addToPocketAndGetID(String url) throws ClientProtocolException, IOException, ParseException {
		HttpClient httpClient = HttpClientBuilder.create().build(); 

		HttpPost request = new HttpPost("https://getpocket.com/v3/add");

		
		String title = "Max Test Pocket";
		String consumerkey = "5572-efaa744e38c7a659a65c423b";
		String accesstoken = "01329d4-ee41-4523-785f-56a378";

		StringEntity params = new StringEntity("{\"url\":\"" + url + "\",\"title\":\"" + title
				+ "\",\"consumer_key\":\"" + consumerkey + "\",\"access_token\":\"" + accesstoken + "\"} ");

		request.addHeader("content-type", "application/json");
		request.setEntity(params);
		HttpResponse response = httpClient.execute(request);
		// System.out.println(EntityUtils.toString(response.getEntity()));

		// handle response here...
		String json = EntityUtils.toString(response.getEntity());
		System.out.println(json);

		JSONParser parser = new JSONParser();

		Object obj = parser.parse(json);
		JSONObject jsonObj = (JSONObject) obj;
		String itemJSON = jsonObj.get("item").toString();
		System.out.println(itemJSON);
		
		Object objItem = parser.parse(itemJSON);
		JSONObject jsonObjItem = (JSONObject) objItem;
		String itemID = jsonObjItem.get("item_id").toString();
		
		return itemID; 

	}

}
