package com.abg.fileproc;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class RequestFileReader {
	public void ReadRequestFile(String reqFile) throws FileNotFoundException, IOException {

		try (BufferedReader br = new BufferedReader(new FileReader(reqFile))) {

			String request = "";

			while ((request = br.readLine()) != null && !request.isEmpty()) {
				if (!request.contains("*")) {
					
					System.out.println("******* Start processing req " + request + " *****");
					UpdateLineINLinksFile.updateLine(reqFile,	request, "*" + request);
					
				} else {
					System.out.println("link contains * ");
				}
			}

			System.out.println("*** ALL FILES ARE SUCCESSFULLY PROCESSED ***");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
