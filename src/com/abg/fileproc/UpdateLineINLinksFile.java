package com.abg.fileproc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class UpdateLineINLinksFile {
	
	public static void updateLine(String linksFile, String toUpdate, String updated) throws IOException {		    
		    
		    BufferedReader file = new BufferedReader(new FileReader(linksFile));
		    String line ="";
		    String input = "";

		    while ((line = file.readLine()) != null) {

		    	if (line.toString().equals(toUpdate)) {
	    			input += "*" + line + "\n";
	    		} else {
	    			input += line + "\n";
	    		}
		    	
		    }
		    
		    //input = input.replace(toUpdate, updated);

		    FileOutputStream os = new FileOutputStream(linksFile);
		    os.write(input.getBytes());

		    file.close();
		    os.close();
		    
		   
		}
		
}