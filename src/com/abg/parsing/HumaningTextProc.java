package com.abg.parsing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.apache.commons.collections.set.SynchronizedSortedSet;
import org.apache.commons.lang3.StringUtils;

import com.abg.services.RandomInt;
import com.abg.services.StringFactory;


public class HumaningTextProc {
	
	public static String getHumanTextForSiteA (ArrayList<String> arrayFromSERPtext) throws IOException, InterruptedException {
		
		ArrayList<String> arrayFromSERPtextForShuffle = new ArrayList<String>();
		arrayFromSERPtextForShuffle.clear();
		
		String textForShuffle = "";
		String textForSite = "";
		
		
		//get string from array
		for (String string : arrayFromSERPtext) {
			textForShuffle = textForShuffle + string;
		}
		
		//get passages before dot(.)
		String[] passages = textForShuffle.split("\\. ");
		
		//add passages to Array to Shuffeling
		for (int it = 0; it< passages.length; it++) {
			String onePassage = passages[it] + ". ";
			String onePassageWithLowCase = onePassage.toLowerCase();		
			String onePassageWithLowCaseANDFirstUp = new StringFactory().upCaseFirstChar(onePassageWithLowCase);
			System.out.println(onePassageWithLowCaseANDFirstUp);
			arrayFromSERPtextForShuffle.add(onePassageWithLowCaseANDFirstUp);
		}		
		
		// shuffle array items
		long seed = System.nanoTime();
		Collections.shuffle(arrayFromSERPtextForShuffle, new Random(seed));
		
		// adding <p> and </p> in start, end text
		arrayFromSERPtextForShuffle.add(0, "<p>");
		arrayFromSERPtextForShuffle.add(arrayFromSERPtextForShuffle.size(), "</p>");
		
		// adding random <p> and </p> in text
		if (arrayFromSERPtextForShuffle.size() > 7 && arrayFromSERPtextForShuffle.size() < 12) {
			System.out.println("ADD 1 </p><p> because arrayFromSERPtextForShuffle.size()= "+ arrayFromSERPtextForShuffle.size());
			int rnd = RandomInt.getRandInt(2, 5);
			arrayFromSERPtextForShuffle.add(rnd, "</p><p>");
		} else if (arrayFromSERPtextForShuffle.size() >= 12 && arrayFromSERPtextForShuffle.size() < 16) {
			System.out.println("ADD 2 <p></p> because arrayFromSERPtextForShuffle.size()= "+ arrayFromSERPtextForShuffle.size());
			int rnd = RandomInt.getRandInt(2, 5);
			int rnd2 = RandomInt.getRandInt(8, 12);
			arrayFromSERPtextForShuffle.add(rnd, "</p><p>");
			arrayFromSERPtextForShuffle.add(rnd2, "</p><p>");
		} else if (arrayFromSERPtextForShuffle.size() >= 16 && arrayFromSERPtextForShuffle.size() < 20) {
			System.out.println("ADD 3 <p></p> because arrayFromSERPtextForShuffle.size()= "+ arrayFromSERPtextForShuffle.size());
			int rnd = RandomInt.getRandInt(2, 5);
			int rnd2 = RandomInt.getRandInt(8, 12);
			arrayFromSERPtextForShuffle.add(rnd, "</p><p>");
			arrayFromSERPtextForShuffle.add(rnd2, "</p><p>");
		} else if (arrayFromSERPtextForShuffle.size() >= 20 && arrayFromSERPtextForShuffle.size() < 35) {
			System.out.println("ADD 4 <p></p> because arrayFromSERPtextForShuffle.size()= "+ arrayFromSERPtextForShuffle.size());
			int rnd = RandomInt.getRandInt(2, 5);
			int rnd2 = RandomInt.getRandInt(8, 12);
			int rnd3 = RandomInt.getRandInt(16, 18);
			arrayFromSERPtextForShuffle.add(rnd, "</p><p>");
			arrayFromSERPtextForShuffle.add(rnd2, "</p><p>");
			arrayFromSERPtextForShuffle.add(rnd3, "</p><p>");
		}
		
		
		// create text
		for (String string : arrayFromSERPtextForShuffle) {
			textForSite = textForSite + string;
		}
		
		System.out.println(textForSite);

		return textForSite;
	}
	
	public static String getHumanTextForSiteAWithH2H3(String humanTextForSiteAFromSERP, ArrayList<String> allH2H3array){
		ArrayList<String> arrayHumanWithH2H2= new ArrayList<String>();
		arrayHumanWithH2H2.clear();
		
		String textForSiteWithH2H3="";
		
		int allH2H3arraySize = allH2H3array.size();
		
		System.out.println("размер allH2H3array="+allH2H3arraySize );
		System.out.println("распаковываю массив...");
		for (int i = 0; i < allH2H3array.size(); i++) {
			System.out.println("значение allH2H3array№" +i+ "равно="+allH2H3array.get(i));
		}
		System.out.println("текст humanTextForSiteAFromSERP="+humanTextForSiteAFromSERP );
	
		//--
		if(humanTextForSiteAFromSERP.contains("</p><p>")){

			int count = StringUtils.countMatches(humanTextForSiteAFromSERP, "</p><p>");
			System.err.println("в тексте "+count+ "сопадений");
			System.out.println("было:"+humanTextForSiteAFromSERP);
			
			String[] pass = humanTextForSiteAFromSERP.split("</p><p>");
			List<String> passagesList = Arrays.asList(pass);
			
			System.out.println("passagesList="+passagesList.size());
			
			int ii = 0;
			for (String str : passagesList) {
				System.out.println("II"+ii);
				
				if(!allH2H3array.isEmpty()){
					
					String insertH2 = allH2H3array.get(RandomInt.getRandInt(0, allH2H3arraySize-1));
					System.out.println("insert"+ insertH2);
					
					int rnd = RandomInt.getRandInt(0, 3);
					System.out.println("rnd"+rnd);
					
					if (rnd==0){
						if(ii < passagesList.size()-1){
							String txt = str+"</p><h2>"+insertH2+"</h2><p>";
							arrayHumanWithH2H2.add(txt);
							System.out.println("txt="+txt);
						} else {
							String txt = str;
							arrayHumanWithH2H2.add(txt);
							System.out.println("txt2="+txt);
						}
					} else if (rnd==1){
						if(ii < passagesList.size()-1){
							String txt = str+"</p><h3>"+insertH2+"</h3><p>";
							arrayHumanWithH2H2.add(txt);
							System.out.println("txt="+txt);
						} else {
							String txt = str;
							arrayHumanWithH2H2.add(txt);
							System.out.println("txt2="+txt);
						}
					} else {
						if(ii < passagesList.size()-1){
							String txt = str+"</p><p>";
							arrayHumanWithH2H2.add(txt);
							System.out.println("txt="+txt);
						} else {
							String txt = str;
							arrayHumanWithH2H2.add(txt);
							System.out.println("txt2="+txt);
						}
					}
				}	else {
					String txt = str+"</p><p>";
					arrayHumanWithH2H2.add(txt);
					System.out.println("txt="+txt);
					System.out.println("шляпа");
				}
					
					ii++;
			}
			
		} else {
			System.out.println("Doesn't have </p>");
		}

		// create text
		for (String string : arrayHumanWithH2H2) {
			textForSiteWithH2H3 = textForSiteWithH2H3 + string;
		}
		
		System.out.println("стало:"+textForSiteWithH2H3);
		return textForSiteWithH2H3;
		
	}

}
