package com.abg.parsing;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.abg.services.RandomInt;

public class SPUTNIKParsSERP {
	
	public static ArrayList<String> getTextFromDescription(String query) throws IOException, InterruptedException {
		
		int randSPUTNIKTimeout = RandomInt.getRandInt(5000,8000);
		System.out.println("SPUTNIK sleep: " + randSPUTNIKTimeout);
		
		
		Thread.sleep(randSPUTNIKTimeout);
		
		ArrayList<String> arr = new ArrayList<String>();
		
		String queryEnc = URLEncoder.encode(query, "UTF-8");
		String url = "http://www.sputnik.ru/search?q="+  queryEnc;

		// file for write response from SERP
		String htmlFile = "saveSERPfileTXT.html";

		// connection
		Response respYa = Jsoup
				.connect(url)
				.ignoreContentType(true)
				.userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
				.timeout(1205868).followRedirects(true).execute();

		// write response to html file
		Writer writer = new PrintWriter(htmlFile);
		writer.write(respYa.body());
		writer.close();
		
		
		Thread.sleep(3000);
		
		// read html file
		File input = new File(htmlFile);
		Document doc = Jsoup.parse(input, "UTF-8");

		if (!doc.title().toString().contains("Ой!")) {
			// pars html file

			if (doc.select("div.b-result-list").select("div.b-result") != null) {
				Elements elements = doc.select("div.b-result-list").select("div.b-result");

				for (Element elementsOfItem : elements) {
					
					if (!elementsOfItem.select("div.b-result").isEmpty()) {
						Elements title = elementsOfItem.select("a");

						if (title.select("a").first() != null) {
							Element linkOfTitle = title.select("a").first();
							String link = linkOfTitle.attr("href");

							Elements divsDesc = elementsOfItem.select("div.b-result-text");
							String desc = divsDesc.first().text();

							// stop-sites
							if (!link.contains("wellnesso")
									&& !link.contains("otvet.mail.ru")
									&& !link.contains("video.search.yahoo.com")
									&& !link.contains("pricheskish.ru")
									&& !link.contains("volpuri.ru")
									&& !link.contains("hairdresser.com.ua")
									&& !link.contains("jurnalik.ru")
									&& !link.contains("akphoto2.ask.fm")
									
									
									&& !desc.toLowerCase().contains("продажа")
									&& !desc.toLowerCase().contains(".ru")
									&& !desc.toLowerCase().contains(".com")
									&& !desc.toLowerCase().contains("заказать")
									&& !desc.toLowerCase().contains("купить")
									&& !desc.toLowerCase().contains("самовывоз")
									&& !desc.toLowerCase().contains("цена")
									&& !desc.toLowerCase().contains("стоимость")
									&& !desc.toLowerCase().contains("цены"))
							{
									arr.add(desc);
							}

						} else {
							//System.out.println("pics or soc or sidebar");
						}

					} else {
						System.out.println("ads");
					}

				}
			} else {
				System.out.println("ooo");
			}
		} else {
			System.out.println("Бан от Яндекса");
		}
		return arr;

	}
}