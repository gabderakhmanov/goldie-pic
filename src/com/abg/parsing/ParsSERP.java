package com.abg.parsing;

import java.io.IOException;
import java.util.ArrayList;

import com.abg.services.DotPaste;
import com.abg.services.RandomInt;

public class ParsSERP {

	public static ArrayList<String> getTextFromSERP(String query) throws IOException, InterruptedException {

		System.out.println("Start parsing SERP");

		ArrayList<String> arrayFromSERPtext = new ArrayList<String>();
		
		int randNumberSERP = RandomInt.getRandInt(1, 1); // get random SERP number

		switch (randNumberSERP) {

		/*		case 0: 
			System.out.println("SERP number 0");
			ArrayList<String> arrBing = BINGParsSERP.getTextFromDescription(queryClear);
			for (String string : arrBing) {
				string = string.replace("...", " ");
				if (!string.isEmpty()) {
					if (!string.endsWith(".") && !string.endsWith("!") && !string.endsWith("?")) {
						// remove more 2 space
						String trimStrToArr = string.replaceAll("[\\s]{2,}", " ");
						// get clear string with dot
						String resultStr = DotPaste.getClearStringWithDot(trimStrToArr);
						arrayFromSERPtext.add(resultStr);
					} else {
						arrayFromSERPtext.add(string + " ");
					}
				}
			}
			System.out.println("Bing all add");
			break;
		case 1:
			System.out.println("SERP number 1");
			ArrayList<String> arrLI = LIParsSERP.getTextFromDescription(queryClear);
			for (String string : arrLI) {
				string = string.replace("...", " ");
				if (!string.isEmpty()) {
					if (!string.endsWith(".") && !string.endsWith("!") && !string.endsWith("?")) {
						// remove more 2 space
						String trimStrToArr = string.replaceAll("[\\s]{2,}"," ");
						// get clear string with dot
						String resultStr = DotPaste.getClearStringWithDot(trimStrToArr);
						mainArray.add(resultStr);
					} else {
						mainArray.add(string + " ");
					}
				}
			}

			System.out.println("LI all add");
			break;
		case 2:
			System.out.println("SERP number 2");
			ArrayList<String> arrYAHOO = YAHOOParsSERP.getTextFromDescription(queryClear);
			for (String string : arrYAHOO) {
				string = string.replace("...", " ");
				if (!string.isEmpty()) {
					if (!string.endsWith(".") && !string.endsWith("!") && !string.endsWith("?")) {
						// remove more 2 space
						String trimStrToArr = string.replaceAll("[\\s]{2,}"," ");
						// get clear string with dot
						String resultStr = DotPaste.getClearStringWithDot(trimStrToArr);
						mainArray.add(resultStr);
					} else {
						mainArray.add(string + " ");
					}
				}
			}

			System.out.println("YAHOO all add");
			break;*/
		case 0:
			System.out.println("SPUTNIK number 3");
			ArrayList<String> arrSPUTNIK1 = SPUTNIKParsSERP.getTextFromDescription(query);
			for (String string : arrSPUTNIK1) {
				string = string.replace("...", " ");
				if (!string.isEmpty()) {
					if (!string.endsWith(".") && !string.endsWith("!") && !string.endsWith("?")) {
						// remove more 2 space
						String trimStrToArr = string.replaceAll("[\\s]{2,}"," ");
						// get clear string with dot
						String resultStr = DotPaste.getClearStringWithDot(trimStrToArr);
						arrayFromSERPtext.add(resultStr);
					} else {
						arrayFromSERPtext.add(string + " ");
					}
				}
			}

			System.out.println("SP all add");
			break;			
		case 1:
			System.out.println("SPUTNIK number 3");
			ArrayList<String> arrSPUTNIK2 = SPUTNIKParsSERP.getTextFromDescription(query);
			for (String string : arrSPUTNIK2) {
				string = string.replace("...", " ");
				if (!string.isEmpty()) {
					if (!string.endsWith(".") && !string.endsWith("!") && !string.endsWith("?")) {
						// remove more 2 space
						String trimStrToArr = string.replaceAll("[\\s]{2,}"," ");
						// get clear string with dot
						String resultStr = DotPaste.getClearStringWithDot(trimStrToArr);
						arrayFromSERPtext.add(resultStr);
					} else {
						arrayFromSERPtext.add(string + " ");
					}
				}
			}

			System.out.println("SPUTNIK all add");
			break;			
		case 2:
			System.out.println("SERP number 3");
			ArrayList<String> arrSPUTNIK3 = SPUTNIKParsSERP.getTextFromDescription(query);
			for (String string : arrSPUTNIK3) {
				string = string.replace("...", " ");
				if (!string.isEmpty()) {
					if (!string.endsWith(".") && !string.endsWith("!") && !string.endsWith("?")) {
						// remove more 2 space
						String trimStrToArr = string.replaceAll("[\\s]{2,}"," ");
						// get clear string with dot
						String resultStr = DotPaste.getClearStringWithDot(trimStrToArr);
						arrayFromSERPtext.add(resultStr);
					} else {
						arrayFromSERPtext.add(string + " ");
					}
				}
			}

			System.out.println("SPUTNIK all add");
			break;
		
		}

		return arrayFromSERPtext;
	}

}
