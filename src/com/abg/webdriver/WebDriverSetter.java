package com.abg.webdriver;

import java.io.File;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class WebDriverSetter {
	
	public static ChromeDriver setChromeDriver() {

		//set ChromeDriver		
		File fileChromedriver = new File("C:\\selenium\\drivers\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", fileChromedriver.getAbsolutePath() );
		
		//set usrProfile
		String userProfile= "c:\\Users\\Tanya\\AppData\\Local\\Google\\Chrome\\User Data\\";
		
		//set options
		ChromeOptions options = new ChromeOptions();
		options.addArguments("user-data-dir="+userProfile);
		options.addArguments("--start-maximized");
		ChromeDriver driver = new ChromeDriver(options);
		
		return driver;
	}

}
