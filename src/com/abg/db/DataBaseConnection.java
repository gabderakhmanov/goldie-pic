package com.abg.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseConnection {
	
	public static Connection connectToDB(String urlDB, String userDB, String passwordDB) throws SQLException {
		
		System.out.println("Connect to DB: " + userDB );
		Connection connect = DriverManager.getConnection(urlDB, userDB, passwordDB);
		System.out.println("Connection to DB OK");
		return connect;

	}

}
