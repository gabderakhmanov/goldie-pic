package com.abg.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.abg.services.RandomInt;

public class WriteToDB {
	
	public static String writeToTablePostAndGetLastPostId(Connection connect, String headerOfPost, String textOfPost, String urlOfPost, String h1FromPocketOfPost) throws SQLException{
		
		//insert
		String sqlPost = "INSERT INTO post VALUES (?,?,?,?,?,?,?,?)";
		
		String cat = String.valueOf(RandomInt.getRandInt(1, 5));
		System.err.println("cat is :"+cat);
			
		PreparedStatement statementPost = connect.prepareStatement(sqlPost);
		statementPost.setString(1,null);
		statementPost.setString(2,headerOfPost);
		statementPost.setString(3,textOfPost);
		statementPost.setString(4,urlOfPost);
		statementPost.setString(5,h1FromPocketOfPost);
		statementPost.setString(6,cat);
		statementPost.setString(7,"0");
		statementPost.setString(8,"0");

		statementPost.executeUpdate();	
		System.out.println("writen to post TABLE");
		
		//get id of last row
		String q = "SELECT * FROM post";
		ResultSet rs = statementPost.executeQuery(q);

		int idOfLastPost = 0;
		if (rs.last()) {
			int id = rs.getInt("id");
			idOfLastPost = idOfLastPost + id;
		}
		String idOfPost = String.valueOf(idOfLastPost);
		return idOfPost;
	}

	
	public static void writeToTableImgofpost(Connection connect, String imgFileName, String postID) throws SQLException{
		
		//insert
		String sqlUrlOnSite = "INSERT INTO imgofpost VALUES (?,?,?)";	
		
		PreparedStatement statementUrlOnSite = connect.prepareStatement(sqlUrlOnSite);
		statementUrlOnSite.setString(1,null);
		statementUrlOnSite.setString(2,imgFileName);
		statementUrlOnSite.setString(3,postID);

		statementUrlOnSite.executeUpdate();	
		System.out.println("writen to imgofpost TABLE");
	}
}
