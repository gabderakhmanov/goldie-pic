package com.abg.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.abg.services.RandomInt;
import com.abg.services.StringFactory;

public class DataBaseWriting {
	public static void writeDataToDB(Connection connect, ArrayList<String> arrayOfIMGLinksAndSrc, String request, int randCategory, String humanTextForSiteAFromSERP) throws SQLException {
		
		int idOfLastRow = 0;
		int idOfLastRowArticle = 0;
		
		// INSERT url on site
		String strRequest = request;
		String urlFromRequest = StringFactory.getUrlFromString(request);
		int defaultPUB = 0;
		int defaultMAP = 0;
		
		String sqlUrlOnSite = "INSERT INTO urlonsite VALUES (?,?,?,?,?)";	
		
		PreparedStatement statementUrlOnSite = connect.prepareStatement(sqlUrlOnSite);
		statementUrlOnSite.setString(1,null);
		statementUrlOnSite.setString(2,strRequest);
		statementUrlOnSite.setString(3,urlFromRequest);
		statementUrlOnSite.setInt(4,defaultPUB);
		statementUrlOnSite.setInt(5,defaultMAP);
		statementUrlOnSite.executeUpdate();	
		System.out.println("urlonsite is writen to DB");
		
		// SELECT url on site
		String q = "SELECT * FROM urlonsite";
		ResultSet rs = statementUrlOnSite.executeQuery(q);

		if (rs.last()) {
			int id = rs.getInt("id");
			idOfLastRow = idOfLastRow + id;
		}
		
		// INSERT link and srce
		for (String strFromArr : arrayOfIMGLinksAndSrc) {
			
			System.out.println(strFromArr);
			
			String delims = "[$]";
			String[] tokens = strFromArr.split(delims);
			
			int randLike = RandomInt.getRandInt(0,15);
			System.out.println(randLike);
			
			if (tokens[0] != null || tokens[1] != null) {

				String tok0 = tokens[0];
				String tok1 = tokens[1];
				int idOfLstRow = idOfLastRow;
				
				String sql = "INSERT INTO imglinks VALUES (?,?,?,?,?,?)";
	
				PreparedStatement statement= connect.prepareStatement(sql);
				statement.setString(1,null);
				statement.setString(2,tok0);
				statement.setString(3,tok1);
				statement.setInt(4,idOfLstRow);
				statement.setInt(5,randCategory);
				statement.setInt(6,randLike);
				
				statement.executeUpdate();
				System.out.println("imglinks is writen to DB");

			} else {
				System.out.println("Bad url OR title of img");
			}	
		}
		
		// SELECT id of article
		String idArticle = "SELECT * FROM urlonsite";
		ResultSet rsidArticle = statementUrlOnSite.executeQuery(idArticle);

		if (rsidArticle.last()) {
			int id = rsidArticle.getInt("id");
			idOfLastRowArticle = idOfLastRowArticle + id;
		}
		
		// INSERT txt content						
		if (humanTextForSiteAFromSERP != null) {

			String textArticle = humanTextForSiteAFromSERP;
			int idarticle = idOfLastRowArticle;
	
			String sql = "INSERT INTO txtcont VALUES (?,?,?)";

			PreparedStatement statement= connect.prepareStatement(sql);
			statement.setString(1,null);
			statement.setString(2,textArticle);
			statement.setInt(3,idarticle);

			statement.executeUpdate();
			System.out.println("txtcont is writen to DB");
			
		}			

		
		connect.close();
		System.out.println("ALL Data to DB is writen");
	}
}
