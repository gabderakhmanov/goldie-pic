package com.abg.serp;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SerpProcessor {

	public static String saveBingSERPHtml(int timeout, String query) throws InterruptedException, IOException {

		System.out.println("Search Name: http://www.bing.com/");
		System.out.println("Waiting ... " + timeout);
		Thread.sleep(timeout);

		String queryEnc = URLEncoder.encode(query, "UTF-8");
		String url = "http://www.bing.com/search?q=" + queryEnc;
		System.out.println(url);

		// file for write response from SERP
		String htmlFile = "htmlFileSERPWithLink.html";

		// connection
		Response resp = Jsoup.connect(url).ignoreContentType(true)
				.userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
				.timeout(1205868).followRedirects(true).execute();

		// write response to html file
		Writer writer = new PrintWriter(htmlFile);
		writer.write(resp.body());
		writer.close();

		System.out.println("Waiting ... " + timeout);
		Thread.sleep(timeout);
		return htmlFile;
	}

	public static String getLinkFromSERPForPocket(String htmlWithLinkSERP) throws IOException {

		System.out.println("getLinkFromSERPForPocket from: " + htmlWithLinkSERP);

		ArrayList<String> linkForPocket = new ArrayList<String>();

		// read html file
		File input = new File(htmlWithLinkSERP);
		Document doc = Jsoup.parse(input, "UTF-8");

		// pars html file

		if (doc.select("ol#b_results").select("li.b_algo") != null) {
			
			Elements elements = doc.select("ol#b_results").select("li.b_algo");
			
			int numOfElement = 0; // num of block in SERP
			
			if(!elements.get(numOfElement).select("li.b_algo").isEmpty()){
				//Elements el = elements.get(numOfElement).select("li.b_algo").select("h2"); horoscopes.rambler.ru
				//System.out.println("el"+el);
				if(elements.get(numOfElement).select("li.b_algo").select("a").first().attr("href") != null ){
					
					while(elements.get(numOfElement).select("li.b_algo").select("a").first().attr("href").contains("mail.ru")
							|| elements.get(numOfElement).select("li.b_algo").select("a").first().attr("href").contains("alltaro.ru")
							|| elements.get(numOfElement).select("li.b_algo").select("a").first().attr("href").contains("gorockop.ru") 
							|| elements.get(numOfElement).select("li.b_algo").select("a").first().attr("href").contains("to-name.ru")
							|| elements.get(numOfElement).select("li.b_algo").select("a").first().attr("href").contains("neolove.ru")
							|| elements.get(numOfElement).select("li.b_algo").select("a").first().attr("href").contains("kakzovut.ru")
							|| elements.get(numOfElement).select("li.b_algo").select("a").first().attr("href").contains("rambler.ru")
							|| elements.get(numOfElement).select("li.b_algo").select("a").first().attr("href").contains("sonnik-online.net")
							|| elements.get(numOfElement).select("li.b_algo").select("a").first().attr("href").contains("astromeridian.ru")) {
						
						System.out.println("num while="+numOfElement);
						System.out.println("STOP lnk="+elements.get(numOfElement).select("li.b_algo").select("a").first().attr("href"));
						numOfElement++;
					} 
					
					String link = elements.get(numOfElement).select("li.b_algo").select("a").first().attr("href");
					System.out.println("Add link ITOG: " + link);
					linkForPocket.add(link);
				}

			}
		}
		
		String linkForPocketString = linkForPocket.get(0);
		System.out.println("linkForPocketString: " + linkForPocketString);
		
		return linkForPocketString;

	}

}
